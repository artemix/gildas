#
# Nicolas Moreau - 20 Juillet 2022
#

#------------------------------------------------------------#
#                                                            #
#                  General required stuff                    #
#                                                            #
#------------------------------------------------------------#

FROM debian:bullseye
RUN apt-get -y update
RUN apt-get -y upgrade

ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get install -y apt-transport-https

RUN apt-get install -y --fix-missing build-essential \
		               nano \
                       bzip2 \
                       curl \
                       g++ \
                       gfortran \
                       pkg-config \
                       python2 \
                       python3-dev \
                       texlive-font-utils \
                       python3 \
                       python3-pip \
                       python-setuptools \
                       tar\
                       lpr \
                       libgtk2.0-dev \
                       libcfitsio-dev

RUN ln -s /usr/bin/python2 /usr/bin/python

RUN pip3  install ansible==2.4.3.0 pandas

VOLUME /data
VOLUME /maps

COPY ./gildas-src-jun22a.tar.xz /gildas/
COPY ./init.sh /gildas/init.sh
RUN tar -xvf /gildas/gildas-src-jun22a.tar.xz
WORKDIR /gildas-src-jun22a
ENV GAG_SEARCH_PATH=/usr/lib:/usr/lib/x86_64-linux-gnu
RUN . admin/gildas-env.sh && make && make install

RUN echo "export GAG_SEARCH_PATH=/usr/lib:/usr/lib/x86_64-linux-gnu \n \
          export GAG_ROOT_DIR=/gildas-exe-jun22a \n \
          export GAG_EXEC_SYSTEM=x86_64-debian11-gfortran \n \
          export GAG_EXEC_DIR="\$GAG_ROOT_DIR/\$GAG_EXEC_SYSTEM" \n \
          export LD_LIBRARY_PATH="\$GAG_EXEC_DIR/lib::\$LD_LIBRARY_PATH" \n \
          source \$GAG_ROOT_DIR/etc/bash_profile" >> /etc/bash.bashrc

ENV PATH=$PATH:/gildas-exe-jun22a/x86_64-debian11-gfortran/bin:/gildas-exe-jun22a/x86_64-debian11-gfortran/lib

WORKDIR /
