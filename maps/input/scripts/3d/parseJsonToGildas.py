#!/usr/bin/python3
import sys
import os
import pandas as pd

#name = 'test-12co10-cube-cube.fits.json'
print("IN PYTHON SCRIPT")
if len(sys.argv) == 3:
    output_dir = sys.argv[1]
    name = sys.argv[2]
    df=pd.read_json(os.path.join(output_dir, name))
    #
    spectrumChannel=df["data"]["upperImage"]["channel"]
    #
    imageXpix=df["data"]["upperImage"]["refPixel"]["x"]
    imageYpix=df["data"]["upperImage"]["refPixel"]["y"]
    #
    boxXpixMin=df["data"]["lowerImage"]["box"]["xmin"]
    boxYpixMin=df["data"]["lowerImage"]["box"]["ymin"]
    boxWidth=df["data"]["lowerImage"]["box"]["width"]
    boxHeight=df["data"]["lowerImage"]["box"]["height"]
    #
    spectrumMinChan=df["data"]["lowerSpectrum"]["imin"]
    spectrumMaxChan=df["data"]["lowerSpectrum"]["imax"]
    #
    print("open  : ")
    print(os.path.join(output_dir, name+".txt"))
    with open(os.path.join(output_dir, name+".txt"), "w") as f:
        s=str(spectrumChannel)+" "+str(imageXpix)+" "+str(imageYpix)+" "+str(boxXpixMin)+" "+str(boxYpixMin)+ \
          " "+str(boxWidth)+" "+str(boxHeight)+" "+str(spectrumMinChan)+" "+str(spectrumMaxChan)
        f.write(s)

else:
    print("This script expects the name of a json file as a parameter")
