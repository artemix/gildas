#!/usr/bin/python3
import sys
import os
import pandas as pd

#name = 'test-12co10-cube-cube.fits.json'
print("### in parseJsonToGildas 2D script")
if len(sys.argv) == 3:
    output_dir = sys.argv[1]
    name = sys.argv[2]
    df=pd.read_json(os.path.join(output_dir, name))
    #
    imageXpix=df["data"]["image"]["refPixel"]["x"]
    imageYpix=df["data"]["image"]["refPixel"]["y"]
    #
    boxXpixMin=df["data"]["image"]["box"]["xmin"]
    boxYpixMin=df["data"]["image"]["box"]["ymin"]
    boxWidth=df["data"]["image"]["box"]["width"]
    boxHeight=df["data"]["image"]["box"]["height"]
    #
    print("open  : ")
    print(os.path.join(output_dir, name+".txt"))
    with open(os.path.join(output_dir, name+".txt"), "w") as f:
        s=f"{imageXpix} {imageYpix} {boxXpixMin} {boxYpixMin} {boxWidth} {boxHeight}"
        f.write(s)

else:
    print("This script expects the name of a json file as a parameter")
