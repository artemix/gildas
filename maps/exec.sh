#!/bin/bash

MAPS_DIR='/maps'
DATA_DIR='/data'
TMP_DIR="$MAPS_DIR/tmp"
INPUT_DIR="$MAPS_DIR/input/scripts"
YAFITS_INPUT_DIR="$MAPS_DIR/input/yafits_data"
OUTPUT_DIR="$MAPS_DIR/output"

#clean run
function clear_dir(){
  rm -rf "$MAPS_DIR"/*.json
  rm -rf "$MAPS_DIR"/*.*~
  rm -rf "$MAPS_DIR"/*.mean
  rm -rf "$MAPS_DIR"/*.velo
  rm -rf "$MAPS_DIR"/*.width
  rm -rf "$MAPS_DIR"/*.init
  rm -rf "$MAPS_DIR"/tmp/*
  rm -rf "$MAPS_DIR"/output/*
  rm "$INPUT_DIR/*.json"
  rm "$INPUT_DIR/*.txt"
}

# run gildas script on a given file
# $1 : file path in /data
# $2 : file name without any suffix
function run_gildas3d(){
  TXT_PARAMS_FILE="$2.json.txt"
  cp "$DATA_DIR/$1/$2.fits" "$TMP_DIR"
  echo -e "\nStart upperImage.map"
  mapping -nw  @ "$INPUT_DIR/3d/upperImage.map" "$TMP_DIR/$2" "$INPUT_DIR/3d/$TXT_PARAMS_FILE"
  echo -e "\nStart lowerImage.map"
  mapping -nw  @ "$INPUT_DIR/3d/lowerImage.map" "$TMP_DIR/$2" "$INPUT_DIR/3d/$TXT_PARAMS_FILE"
  echo -e  "\nStart upperSpectrum.map"
  mapping -nw  @ "$INPUT_DIR/3d/upperSpectrum.map" "$TMP_DIR/$2" "$INPUT_DIR/3d/$TXT_PARAMS_FILE"
  echo -e "\nStart lowerSpectrum.map"
  mapping -nw  @ "$INPUT_DIR/3d/lowerSpectrum.map" "$TMP_DIR/$2" "$INPUT_DIR/3d/$TXT_PARAMS_FILE"
  #rm $TMP_DIR/*

  mkdir "$OUTPUT_DIR/3d"
  mkdir "$OUTPUT_DIR/3d/$2"
  mv "$MAPS_DIR/gildas_lower_image.json" "$OUTPUT_DIR/3d/$2/lower_image.json"
  mv "$MAPS_DIR/gildas_upper_image.json" "$OUTPUT_DIR/3d/$2/upper_image.json"
  mv "$MAPS_DIR/gildas_lower_spectrum.json" "$OUTPUT_DIR/3d/$2/lower_spectrum.json"
  mv "$MAPS_DIR/gildas_upper_spectrum.json" "$OUTPUT_DIR/3d/$2/upper_spectrum.json"
  #mv "$TMP_DIR/$2-extract-goview.png" "$OUTPUT_DIR/3d/$2/$2-goview.png"
  mv "$TMP_DIR/$2-goview.png" "$OUTPUT_DIR/3d/$2/$2-goview.png"
}

# run gildas script on a given file
# $1 : file path in /data
# $2 : file name without any suffix
function run_gildas2d(){
  TXT_PARAMS_FILE="$2.json.txt"
  cp "$DATA_DIR/$1/$2.fits" "$TMP_DIR"
  ls $INPUT_DIR/2d/$TXT_PARAMS_FILE
  echo -e "\nStart image.map"
  mapping -nw  @ "$INPUT_DIR/2d/image.map" "$TMP_DIR/$2" "$INPUT_DIR/2d/$TXT_PARAMS_FILE"
  rm "$TMP_DIR/$2"

  mkdir "$OUTPUT_DIR/2d"
  mkdir "$OUTPUT_DIR/2d/$2"
  mv "$MAPS_DIR/gildas_2D_image.json" "$OUTPUT_DIR/2d/$2/image.json"
}

# run gildas script on a given file
# $1 : file path in /data
# $2 : file name without any suffix
function run_gildas1d(){
  #TXT_PARAMS_FILE="$2.json.txt"
  cp "$DATA_DIR/$1/$2.fits" "$TMP_DIR"
  echo -e "\nStart spectrum.map"
  mapping -nw  @ "$INPUT_DIR/1d/spectrum.map" "$TMP_DIR/$2" #"$INPUT_DIR/1d/$TXT_PARAMS_FILE"
  rm "$TMP_DIR/$2"

  mkdir "$OUTPUT_DIR/1d"
  mkdir "$OUTPUT_DIR/1d/$2"
  mv "$MAPS_DIR/gildas_spectrum.json" "$OUTPUT_DIR/1d/$2/spectrum.json"
}

cd "$MAPS_DIR"
# clean directory from previous execution
clear_dir


# process all IRAM 3d files
echo -e "## Start 3D cubes"
for file in /data/restricted/iram/all/3d/*; do
  FITS_FILE=`basename $file`
  #removes fits extension
  BASENAME=${FITS_FILE/.fits/}
  #appends json extension
  JSON_FILE="$BASENAME.json"

  echo "copy command"
  echo "cp $YAFITS_INPUT_DIR/3d/$JSON_FILE $INPUT_DIR/3d"
  cp "$YAFITS_INPUT_DIR/3d/$JSON_FILE" $INPUT_DIR/3d
  # generates txt files with parameters for .maps scripts
  # called is BASENAME.json.txt
  $INPUT_DIR/3d/parseJsonToGildas.py $INPUT_DIR/3d $JSON_FILE
  run_gildas3d restricted/iram/all/3d "$BASENAME"
done

# process all IRAM 1d files
echo -e "## Start 1D cubes"
for file in /data/restricted/iram/all/1d/*; do
  FITS_FILE=`basename $file`
  #removes fits extension
  BASENAME=${FITS_FILE/.fits/}
  #appends json extension
  JSON_FILE="$BASENAME.json"

  cp "$YAFITS_INPUT_DIR/$JSON_FILE" $INPUT_DIR/1d
  # generates txt files with parameters for .maps scripts
  # called is BASENAME.json.txt
  run_gildas1d restricted/iram/all/1d "$BASENAME"
done

# process all IRAM 2d files
echo -e "## Start 2D cubes"
for file in /data/restricted/iram/all/2d/*; do
  FITS_FILE=`basename $file`
  #removes fits extension
  BASENAME=${FITS_FILE/.fits/}
  #appends json extension
  JSON_FILE="$BASENAME.json"

  cp "$YAFITS_INPUT_DIR/2d/$JSON_FILE" $INPUT_DIR/2d
  # generates txt files with parameters for .maps scripts
  # called is BASENAME.json.txt
  $INPUT_DIR/2d/parseJsonToGildas.py $INPUT_DIR/2d $JSON_FILE
  run_gildas2d restricted/iram/all/2d "$BASENAME"
done





