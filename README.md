# Gildas docker container

## Name
Yafits Test suite

## Last update of this document
14/02/2024

## Getting started

This is a docker container based on Debian Bullseye aiming at running Gildas with its graphical interface. This container should be installed on a server which will be accessed through ssh -X.
The user will connect to the server and then access the container. 

The Gildas code source file must be placed in the same directory as the Dockerfile and named gildas-src.tar.xz.
This application has been tested with gildas-src-jun22a.tar.xz

This container also embeds some scripts to extract data from datacube for test purpose.

## Building the container

Execute the command : 

docker build . -t gildas

This will compile Gildas which can take about 1 hour.

## Configuring the container

Two paths must be configured in the start.sh script.

- DATA_PATH: path to the actual data directory on the host (the same one used in Yafits)
- GILDAS_DIRECTORY: path of the directory containing this script

## Starting the container

Execute start.sh, this will log you inside the container.

Two volumes are mounted inside this container and are available at the following paths.

    - /data : it contains the scientific data. The path can be updated directly in the start.sh in the DATA_PATH variable.
    - /maps : it contains the gildas .map scripts (this is the /maps directory in the current folder and
    is not meant to be changed)

The container is called "gildas".

## Launching Gildas

First go into /gildas and execute : source /gildas/init.sh
This will export the required environment variables.

Now you can use the usual Gildas commands : class, mapping, greg

## Executing the test scripts

Go into the /maps directory and execute the exec.sh command. The data cubes that 
will be used by the script are configured in it.

The exec.sh command executes source /gildas/init.sh so it is useless to perform it manually
in this case.

The results will be written in the output directory. One folder will be created for 
each fits cube analyzed. They will be created in a upper level directory corresponding to
the data cube type (1d, 2d, 3d).

### Input directory 

#### 1d data

 - spectrum.map : a gildas script to extract data from a 1d cube according to a json parameter file. Result is a json file.

#### 2d data

 - image.map : a gildas script to extract data from a 2d cube according to a json parameter file. Result is a json file.
 - parseJsonToGildas.py : a python script that generates a parameter file for image.map. Data are read from the json file 
   extracted from yafits. The parameter file consist in one line with the followoing values separated by a blank character : 

   {Reference pixel X} {Reference pixel Y} {box Xmin} {box Ymin} {box Width} {box Height}


#### 3d data

 - upperImage.map : a gildas script to get data from a 3d cube for the upper image ( image for a given channel ) 
 - lowerImage.map : a gildas script to get data from a 3d cube for the lower image ( image summed for  several channels ) 
 - upperSpectrum.map : a gildas script to get data from a 3d cube for the upper spectrum ( spectrum for one pixel ) 
 - lowerSpectrum.map : a gildas script to get data from a 3d cube for the lower spectrum ( spectrum for a box on the summed channels image ), it creates a screenshot of the Gildas view for this cube
 - parseJsonToGildas.py : a python script that generates a parameter file for all the above map scripts. Data are read from the json file extracted from yafits. The parameter file consist in one line with the followoing values separated by a blank character : 
        {upper spectrum channel} {upper image reference pixel X} {upper image reference pixel X} {lower image box Xmin} {lower image box Ymin} {lower image box width} {lower image box height} {spectrum channel min} {spectrum channel max}


### Output directory

#### 1d data

Each folder will contain a spectrum.json file.


#### 2d data

Each folder will contain a image.json file.

#### 3d data

Each folder will contain the following files:

    - lower_image.json  
    - lower_spectrum.json 
    - upper_image.json  
    - upper_spectrum.json
    - a screenshot



