#!/bin/bash

# remove socket for display
rm -rf ./display

# Prepare target env
DATA_PATH="/juliette/datartemix/ALMA/FITS"
GILDAS_DIRECTORY="/obs/partemix/gildas"
MAPS_DIRECTORY="$GILDAS_DIRECTORY/maps"
CONTAINER_DISPLAY="0"
CONTAINER_HOSTNAME="xterm"

# Create a directory for the socket
mkdir -p display/socket
touch display/Xauthority

# Get the DISPLAY slot
DISPLAY_NUMBER=$(echo $DISPLAY | cut -d. -f1 | cut -d: -f2)

# Extract current authentication cookie
AUTH_COOKIE=$(xauth list | grep "^$(hostname)/unix:${DISPLAY_NUMBER} " | awk '{print $3}')

# Create the new X Authority file
xauth -f display/Xauthority add ${CONTAINER_HOSTNAME}/unix:${CONTAINER_DISPLAY} MIT-MAGIC-COOKIE-1 ${AUTH_COOKIE}

# Proxy with the :0 DISPLAY
socat UNIX-LISTEN:display/socket/X${CONTAINER_DISPLAY},fork TCP4:localhost:60${DISPLAY_NUMBER} &

# Launch the container for remote ssh access 
#docker run -it --rm --name gildas -e DISPLAY=:${CONTAINER_DISPLAY} -e XAUTHORITY=/tmp/.Xauthority -v ${PWD}/display/socket:/tmp/.X11-unix -v ${PWD}/display/Xauthority:/tmp/.Xauthority  --hostname ${CONTAINER_HOSTNAME} -v ${DATA_PATH}:/data  -v $MAPS_DIRECTORY:/maps gildas

# Launch the container for local use
xhost +local:`docker inspect --format='{{ .Config.Hostname }}' gildas`
docker run --network=host -it --rm -e DISPLAY=${DISPLAY} -v /tmp/.X11-unix:/tmp/.X11-unix gildas
