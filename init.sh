#!/bin/bash
export GAG_ROOT_DIR=/gildas-exe
export GAG_EXEC_SYSTEM=x86_64-debian11-gfortran
export GAG_EXEC_DIR="${GAG_ROOT_DIR}/${GAG_EXEC_SYSTEM}"
export LD_LIBRARY_PATH="${GAG_EXEC_DIR}/lib::${LD_LIBRARY_PATH}"
source $GAG_ROOT_DIR/etc/bash_profile
